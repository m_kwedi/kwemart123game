# 123 Game install
- git clone https://bitbucket.org/m_kwedi/kwemart123game.git	
- cd kwemart123game
- yarn
- yarn start (or open **public folder** and run **index.html**)
- open localhost:3000 (or change the port inside **gulpfile.js** then **yarn start** open localhost:[your port])
- drag and drop to the grid to play.

Nb: be sure to have yarn installed globally or install using **npm install --global yarn**